/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

enum FilterlistLanguage: String {
    case english = "en"
    case german = "de"
    case spanish = "es"
    case french = "fr"
    case italian = "it"
    case dutch = "nl"
    case russian = "ru"
    case chinese = "zh"
    case vietnamese = "vn"
}

struct Filterlist {
    let title: String
    let description: String
    let files: [String]
    let language: FilterlistLanguage
    let tag: Int
}

struct Filterlists {
    static let filterlists = [
        Filterlist(title: "English",
                   description: "(Easylist)",
                   files: ["easylist_min_content_blocker+safari15",
                           "easylist_min+exceptionrules_content_blocker+safari15"],
                   language: .english,
                   tag: 0),
        Filterlist(title: "Deutsch + English",
                   description: "(EasyList Germany+EasyList)",
                   files: ["easylist+easylistgermany-minified+safari15",
                           "easylist+easylistgermany-minified+exceptionrules-minimal+safari15"],
                   language: .german,
                   tag: 1),
        Filterlist(title: "español + English",
                   description: "(EasyList Spanish+EasyList)",
                   files: ["easylist+easylistspanish-minified+safari15",
                           "easylist+easylistspanish-minified+exceptionrules-minimal+safari15"],
                   language: .spanish,
                   tag: 2),
        Filterlist(title: "français + English",
                   description: "(Liste FR+EasyList)",
                   files: ["easylist+liste_fr-minified+safari15",
                           "easylist+liste_fr-minified+exceptionrules-minimal+safari15"],
                   language: .french,
                   tag: 3),
        Filterlist(title: "italiano + English",
                   description: "(EasyList Italy+EasyList)",
                   files: ["easylist+easylistitaly-minified+safari15",
                           "easylist+easylistitaly-minified+exceptionrules-minimal+safari15"],
                   language: .italian,
                   tag: 4),
        Filterlist(title: "Nederlands + English",
                   description: "(EasyList Dutch+EasyList)",
                   files: ["easylist+easylistdutch-minified+safari15",
                           "easylist+easylistdutch-minified+exceptionrules-minimal+safari15"],
                   language: .dutch,
                   tag: 5),
        Filterlist(title: "русский, українська + English",
                   description: "(RuAdList+EasyList)",
                   files: ["easylist+ruadlist-minified+safari15",
                           "easylist+ruadlist-minified+exceptionrules-minimal+safari15"],
                   language: .russian,
                   tag: 6),
        Filterlist(title: "中文 + English",
                   description: "(EasyList China+EasyList)",
                   files: ["easylist+easylistchina-minified+safari15",
                           "easylist+easylistchina-minified+exceptionrules-minimal+safari15"],
                   language: .chinese,
                   tag: 7),
        Filterlist(title: "Vietnamese + English",
                   description: "(EasyList Vietnam+EasyList)",
                   files: ["easylist+abpvn-minified+safari15",
                           "easylist+abpvn-minified+exceptionrules-minimal+safari15"],
                   language: .vietnamese,
                   tag: 8)
    ]
}
