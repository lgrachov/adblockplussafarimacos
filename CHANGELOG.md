# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.2] - 2022-06-14
### Added
* Added Vietnamese filter lists
* Updated app icon
* Updated macOS deployment target to 10.15 Catalina
* [Updated Filter Lists to new Safari 15 variants](https://gitlab.com/eyeo/filterlists/contentblockerlists/-/commit/f7d67ef0bd55bcf5ce0ef33eaf17273d7f374d90)

## [2.1.16] - 2022-01-25
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/c1027db16b5082a2587b2bcc8478756134178423)

## [2.1.15] - 2022-01-11
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/6808ee13e1bad1c3123bff32848ce41e767b12c1)

## [2.1.14] - 2021-12-02
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/55ec9c5cb39eab56afdb90110a1723405eb1e7a2)

## [2.1.13] - 2021-11-07
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/70714f2d648873d52c3522f4dad339b4384cb725)

## [2.1.12] - 2021-09-14
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/04b4dab79a32a9eb44f50a01962233548b65d948)

## [2.1.11] - 2021-06-07
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/d42ec7cdfc5f5bb06096126441e2cf1eb7313e10)

## [2.1.10] - 2021-05-14
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/6454f344a881f3dc65cdc4829ed554e35488b927)

## [2.1.9] - 2021-05-04
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/36ba81a5c972322a1ab25019b3b781295f936f14)
### Changed
* Changed references of `whitelist` to`allowlist`

## [2.1.8] - 2021-01-26
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/10783d3959e48fea1fc358dd8e4420e0d0ee5b3f)

## [2.1.7] - 2020-12-01
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/af8d1d767cfb33e98319901e4649579eda60e0da)

## [2.1.6] - 2020-11-09
### Added
* Built for Apple Silicon

## [2.1.5] - 2020-10-23
### Added
* Updated Filter Lists

## [2.1.4] - 2020-07-18
### Added
* Updated Filter Lists

## [2.1.3] - 2020-06-30
### Added
* Updated Filter Lists

## [2.1.2] - 2020-04-14
### Added
* Logic for Firebase crash reporting
* UI Fixes
* Updated Filter Lists
### Removed
* Logic for Fabric event tracking

## [2.1.1] - 2019-12-03
### Added
* UI for analytics opt-in
* Logic for Fabric event tracking & crash reporting
### Changed
* Updated Filter Lists

## [2.1] - 2019-11-22
### Added
* Localization into Chinese, Dutch, French, German, Italian, Russian and Spanish
* Added regional filterlists for Chinese, Dutch, French, German, Italian, Russian and Spanish locales
* Dark Mode support
* System Accent Color support
* Groundwork for Firebase integration
### Changed
* Onboarding to presented as modal window
### Removed
* Google+ integration
### Fixed
* Whitelist input validation & duplication errors
* Missing NSWindow title
* Minor UI tweaks
* Minor strings tweak

## [2.0.6] - 2019-05-01
### Changed
* Updated Filter Lists

## [2.0.5] - 2019-04-08
### Changed
* Updated Filter Lists

## [2.0.4] - 2018-12-03
### Changed
* Updated Filter Lists

## [2.0.3] - 2018-10-02
### Changed
* Updated Filter Lists

## [2.0.2] - 2018-09-27
### Added
##### Host App
* App now loads new filter lists between app updates
### Fixed
##### Host App
* Filter list not loading on initial install in some instances
* Incorrect menu tab highlighting on macOS Sierra
* Pre-roll ads & in-video ads on youtube.com
### Changed
##### Host App
* Deployment target to 10.12.6 from 10.12
* On-boarding for legacy users to see toolbar installation procedure
* Styling of on-boarding slides
##### Toolbar & Content Blocker Extensions
* Toolbar icon hover text
* Toolbar extension Description
* ABP Content Blocker & Toolbar extensions names:
  * Adblock Plus is now ABP
  * Adblock Plus Icon is now ABP Control Panel
  * On-boarding assets updated to reflect new naming
  * On-boarding copy updated to reflect new naming

## [2.0.1] - 2018-09-19
### Fixed
##### Host App
- Now updates UI when whitelist updated from Toolbar

## [2.0.0] - 2018-09-19
### Added
##### Host App
* On-boarding instructions
* Acceptable Ads toggle
* User controlled whitelisting
* Help Page with support/social links
##### Safari Toolbar Extension
* User controlled whitelisting
##### Safari Content Blocker Extension
* Automated removal of legacy ABP extension